/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aomsinjook.gameox;

/**
 *
 * @author cherz__n
 */
import java.util.Scanner;

public class OX {

    static boolean isFinish = false;
    static char winner = '-';
    static int row, col;
    static Scanner sc = new Scanner(System.in);

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };
    static char player = 'X';

    static void ShowWelcom() {
        System.out.println("Welcom to OX Game");

    }

    static void ShowTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);

            }
            System.out.println("");

        }

    }

    static void ShowTurn() {
        System.out.println(player + " turn");

    }

    static void ShowInput() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error");

        }
    }

    static void CkhecRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;

    }

    static void CkhecColum() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;

    }

    static void ShowDawe() {

    }
    
    static void CheckX() {

    }


    static void ShowCheckwin() {

    }

    static void SwitchPlayer() {
        if (player == 'X') {
            player = 'O';

        } else {
            player = 'X';
        }

    }

    static void ShowResult() {

    }

    static void ShowBye() {

    }

    public static void main(String[] args) {
        ShowWelcom();
        do {
            ShowTable();
            ShowTurn();
            ShowInput();
            ShowCheckwin();
            SwitchPlayer();
            SwitchPlayer();
        } while (!isFinish);
        ShowResult();
        ShowBye();

    }
}
